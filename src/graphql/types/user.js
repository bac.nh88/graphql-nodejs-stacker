import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../utils';

export const UserType = new GraphQLObjectType({
	name: 'User',
	description: 'Single User Type',
	fields: () => ({
		fullname: Resolvers.string(),
		email: Resolvers.string(),
		phonenum: Resolvers.string(),
		pwd: Resolvers.string(),
	})
});
