import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { Primitives } from '../../utils';
import { AccessTokenType, UserType } from '../types';
import UserModel from '../../model/user';

export const changePwd = {
	type: UserType,
	description: 'change password',
	args: {
		oldPassword: Primitives.requiredString(),
		newPassword: Primitives.requiredString()
	},
	resolve: async (rootValue, { oldPassword, newPassword }, { req }) => {
		try {
            const { user: authUser } = req;
			const hash = bcrypt.hashSync(newPassword, 10);
            let userFind = await UserModel.findById(authUser._id);

            const comparePassword = await userfind.comparePassword(oldPassword);
            if (!comparePassword) {
                return Promise.reject(new Error(req.t('wrongCredential')));
            }
            // const res = await UserModel.updateOne({_id : authUser._id}, {password: hash})
            var query = {'_id': authUser._id};
			userFind.password = hash;

            UserModel.findOneAndUpdate(query, userFind);
            const email = userFind.email;
			return {
                email
			}
        } catch (e) {
			return Promise.reject(e);
		}
	}
};

export const signUp = {
	type: AccessTokenType,
	description: 'Sign up an user',
	args: {
		email: Primitives.requiredString(),
		password: Primitives.requiredString(),
		fullname: Primitives.requiredString(),
		phonenum: Primitives.requiredString()
	},
	resolve: async (rootValue, { email, password, fullname, phonenum }, { req }) => {
		try {
			const user = await UserModel.emailExist(email);
			if (user) {
				return Promise.reject(new Error(req.t('emailTaken')));
			}
			const hash = bcrypt.hashSync(password, 10),
				newUser = await new UserModel({
					email,
					password: hash,
					fullname,
					phonenum
				}).save(),
				accessToken = jwt.sign(
					{ userId: newUser._id },
					process.env.JWT_SECRET,
					{ expiresIn: process.env.JWT_EXPIRATION }
				);
			return {
				accessToken
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};