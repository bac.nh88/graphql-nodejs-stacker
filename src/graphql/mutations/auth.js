import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { Primitives } from '../../utils';
import { AccessTokenType, UserType } from '../types';
import UserModel from '../../model/user';
import mailService from '../../utils/mailService'

export const signIn = {
	type: UserType,
	description: 'Sign in an user',
	args: {
		email: Primitives.requiredString(),
		password: Primitives.requiredString()
	},
	resolve: async (rootValue, { email, password }, { req }) => {
		try {
			const user = await UserModel.emailExist(email);
			if (!user) {
				return Promise.reject(new Error(req.t('wrongCredential')));
			}
			const comparePassword = await user.comparePassword(password);
			if (!comparePassword) {
				return Promise.reject(new Error(req.t('wrongCredential')));
			}
			const fullName = user.fullName;
			const pwd = user.password;
			const phoneNum = user.phoneNum;
			const accessToken = jwt.sign(
				{ userId: user._id },
				process.env.JWT_SECRET,
				{ expiresIn: process.env.JWT_EXPIRATION }
			);
			return {
				email,
				phoneNum,
				fullName,
				pwd
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const signUp = {
	type: AccessTokenType,
	description: 'Sign up an user',
	args: {
		email: Primitives.requiredString(),
		password: Primitives.requiredString(),
		fullName: Primitives.requiredString(),
		phoneNum: Primitives.requiredString()
	},
	resolve: async (rootValue, { email, password, fullName, phoneNum }, { req }) => {
		try {

			const user = await UserModel.emailExist(email);
			if (user) {
				return Promise.reject(new Error(req.t('emailTaken')));
			}
			const hash = bcrypt.hashSync(password, 10),
				newUser = await new UserModel({
					email,
					password: hash,
					fullName,
					phoneNum
				}).save(),
				accessToken = jwt.sign(
					{ userId: newUser._id },
					process.env.JWT_SECRET,
					{ expiresIn: process.env.JWT_EXPIRATION }
				);
			mailService(email);
			return {
				accessToken
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};
