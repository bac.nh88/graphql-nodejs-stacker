export const isAuthenticated = (root, args, context) => {
	if (!context.req.user) {
		return new Error('Not authenticated')
	}
};
