export const MailConfigs = {
    service: process.env.MAIL_SERVICE,
    account: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
    }
};