import nodemailer from 'nodemailer';
import {MailConfigs} from '../utils/config';

 const mailService = (email) => {

    var mailOptions = {
        from: "stacker <stackervn@outlook.com>", // sender address
        to: email, // list of receivers
        subject: 'Hello ', // Subject line
        text: 'Hello world ?', // plain text body
        html: '<b>Hello world ?</b>' // html body
    };


     var mailer = nodemailer.createTransport(MailConfigs);

    mailer.sendMail(mailOptions, (error, response)=>{
        if(error){
            console.log('mail not sent \n',error);
        }
        else{
            console.log("Message sent: " ,response.message);
        }
    });
}

export default mailService;