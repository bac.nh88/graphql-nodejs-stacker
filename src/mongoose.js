import mongoose from 'mongoose';

const connection = "mongodb+srv://admin:123@cluster0-mdogo.mongodb.net/cluster0?retryWrites=true&w=majority";

mongoose
	.connect(connection, {useNewUrlParser: true, useUnifiedTopology: true, connectTimeoutMS: 5000})
	.catch(console.log);

mongoose.connection.on('open', () => console.log('Mongo DB connected'));
